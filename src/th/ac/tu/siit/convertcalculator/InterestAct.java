package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestAct extends Activity
implements OnClickListener {
	float interestRate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest2);

		
		Button b1 = (Button)findViewById(R.id.calculate);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.setting);
		b2.setOnClickListener(this);
		
		TextView tvRate = (TextView)findViewById(R.id.interest);
		interestRate = Float.parseFloat(tvRate.getText().toString());
		
		Intent i = this.getIntent();
		float interest = i.getFloatExtra("defaultInterest", 1.0f);
		//getFloatExtra is used to receive the value from Main Activity
		//We need to specify its[name], and the [default value]
		//the default value is used when no value was sent.
		EditText money= (EditText)findViewById(R.id.money);
		//set decimal number
		money.setText(String.format(Locale.getDefault(), "%.3f", interest));
		EditText year= (EditText)findViewById(R.id.year);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.calculate){
			EditText money = (EditText)findViewById(R.id.money);
			EditText year = (EditText)findViewById(R.id.year);
			TextView amount = (TextView)findViewById(R.id.amount);
			String res = "";
			try {
				float money2 = Float.parseFloat(money.getText().toString());
				float year2 = Float.parseFloat(year.getText().toString());
				float sum = (float) (money2 * Math.pow((interestRate/100)+1, year2));
				res = String.format(Locale.getDefault(), "%.3f", sum);
			} catch(NumberFormatException e1) {
				res = "Invalid input";
			} catch(NullPointerException e2) {
				res = "Invalid input";
			}
			amount.setText(res);
		}
		else if (id == R.id.setting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			//9999 is a request code. It's a user-defined unique integer.
			//It is used to identify the return value
			startActivityForResult(i, 9999);
		}
	}
		//for receiving returned value
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (requestCode == 9999 && resultCode == RESULT_OK) {
				interestRate = data.getFloatExtra("editInterest", 10.0f);
				TextView tvRate = (TextView)findViewById(R.id.interest);
				tvRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
			}
		}
	}

